package com.bank.mock.integration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(MockIntegrationApplication.class, args);
	}

}
